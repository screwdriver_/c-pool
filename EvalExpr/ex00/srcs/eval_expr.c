/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   eval_expr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/18 11:23:46 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/18 17:45:36 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/eval_expr.h"

void	skip_spaces(char **str)
{
	while (**str == ' ')
		(*str)++;
}

int		parse_others(char **str)
{
	int nbr;

	skip_spaces(str);
	if (**str == '(')
	{
		(*str)++;
		nbr = parse_sums(str);
		if (**str == ')')
			(*str)++;
		return (nbr);
	}
	nbr = ft_atoi(*str);
	while (**str >= '0' && **str <= '9')
		(*str)++;
	return (nbr);
}

int		parse_factors(char **str)
{
	int		n1;
	int		n2;
	char	op;

	n1 = parse_others(str);
	while (1)
	{
		skip_spaces(str);
		op = **str;
		if (op != '*' && op != '/' && op != '%')
			return (n1);
		(*str)++;
		n2 = parse_others(str);
		if (op == '*')
			n1 *= n2;
		else if (op == '/')
			n1 /= n2;
		else
			n1 %= n2;
	}
}

int		parse_sums(char **str)
{
	int		n1;
	int		n2;
	char	op;

	n1 = parse_factors(str);
	while (1)
	{
		skip_spaces(str);
		op = **str;
		if (op != '+' && op != '-')
			return (n1);
		(*str)++;
		n2 = parse_factors(str);
		if (op == '+')
			n1 += n2;
		else
			n1 -= n2;
	}
}

int		eval_expr(char *str)
{
	return (parse_sums(&str));
}
