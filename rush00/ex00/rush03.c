/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush03.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/04 17:25:33 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/04 17:35:30 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c);

void	ft_border_line(int width)
{
	int i;

	i = 0;
	while (i < width)
	{
		if (i == 0)
			ft_putchar('A');
		else if (i == width - 1)
			ft_putchar('C');
		else
			ft_putchar('B');
		i++;
	}
}

void	ft_middle_line(int width)
{
	int i;

	i = 0;
	while (i < width)
	{
		if (i == 0 || i == width - 1)
			ft_putchar('B');
		else
			ft_putchar(' ');
		i++;
	}
}

void	rush(int x, int y)
{
	int i;

	if (x <= 0 || y <= 0)
		return ;
	i = 0;
	while (i < y)
	{
		if (i == 0 || i == y - 1)
			ft_border_line(x);
		else
			ft_middle_line(x);
		ft_putchar('\n');
		i++;
	}
}
