/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split_whitespaces.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/10 02:00:12 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/10 02:00:28 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>

int				ft_is_space(char c)
{
	return (c == ' ' || c == '\t' || c == '\n');
}

unsigned int	ft_count_words(char *str)
{
	int i;

	i = 0;
	while (*str)
	{
		while (*str && ft_is_space(*str))
			str++;
		if (!(*str))
			break ;
		i++;
		while (*str && !ft_is_space(*str))
			str++;
	}
	return (i);
}

unsigned int	ft_get_word_length(char *str)
{
	int i;

	i = 0;
	while (*str)
	{
		if (ft_is_space(*str))
			break ;
		str++;
		i++;
	}
	return (i);
}

void			ft_put_word(char **dest, char *src, unsigned int length)
{
	unsigned int i;

	*dest = (char*)malloc(sizeof(char) * (length + 1));
	i = 0;
	while (i < length)
	{
		(*dest)[i] = *src;
		src++;
		i++;
	}
	(*dest)[i] = '\0';
}

char			**ft_split_whitespaces(char *str)
{
	unsigned int	words;
	char			**s;
	unsigned int	i;
	unsigned int	length;

	words = ft_count_words(str);
	s = (char**)malloc(sizeof(char*) * (words + 1));
	i = 0;
	while (i < words)
	{
		while (*str && ft_is_space(*str))
			str++;
		length = ft_get_word_length(str);
		if (!length)
		{
			str++;
			i++;
			continue;
		}
		ft_put_word(s + i, str, length);
		str += length;
		i++;
	}
	s[i] = 0;
	return (s);
}
