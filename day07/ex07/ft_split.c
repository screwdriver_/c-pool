/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/09 23:15:21 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/10 01:48:01 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int				ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

char			*ft_strstr(char *str, char *to_find, unsigned int n)
{
	int length;
	int length2;
	int i;
	int j;

	length = ft_strlen(str);
	length2 = ft_strlen(to_find);
	if (length2 == 0)
		return (str);
	i = n;
	while (i < length)
	{
		j = 0;
		while (j < length2 && str[i + j] == to_find[j])
		{
			if (i + j >= length)
				return (NULL);
			if (j == length2 - 1)
				return (str + i);
			j++;
		}
		i++;
	}
	return (NULL);
}

unsigned int	ft_get_count(char *str, char *charset)
{
	char			*c;
	unsigned int	n;
	unsigned int	count;

	n = 0;
	count = 0;
	while ((c = ft_strstr(str, charset, n)))
	{
		n = (c - str) + 1;
		count++;
	}
	return (count);
}

void			ft_insert(char *dest, char *src, unsigned int size)
{
	unsigned int i;

	i = 0;
	while (i < size)
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
}

char			**ft_split(char *str, char *charset)
{
	unsigned int	count;
	char			*c;
	char			**tab;
	unsigned int	i;
	unsigned int	n;

	count = ft_get_count(str, charset) - 1;
	tab = (char**)malloc(count + 1);
	i = 0;
	n = 0;
	while ((c = ft_strstr(str, charset, n)))
	{
		if (!(c + ft_strlen(charset)))
			break ;
		tab[i] = malloc(c - str - n + 1);
		ft_insert(tab[i], c + ft_strlen(charset), c - str - n);
		i++;
		n = c - str + 1;
	}
	tab[count] = 0;
	return (tab);
}
