/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/07 19:36:18 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/08 18:24:47 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>

int				*ft_range(int min, int max)
{
	int *tab;
	int i;
	int j;

	if (min >= max)
		return ((void*)0);
	if (!(tab = (int*)malloc((max - min) * sizeof(int))))
		return ((void*)0);
	i = min;
	j = 0;
	while (i < max)
	{
		tab[j] = i;
		i++;
		j++;
	}
	return (tab);
}
