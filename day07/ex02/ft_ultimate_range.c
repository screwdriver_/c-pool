/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimate_range.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/07 19:48:07 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/08 20:27:04 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>

int	ft_ultimate_range(int **range, int min, int max)
{
	int				i;
	unsigned int	j;

	if (min >= max)
	{
		*range = ((void*)0);
		return (0);
	}
	if (!(*range = (int*)malloc((max - min) * sizeof(int))))
	{
		*range = ((void*)0);
		return (0);
	}
	i = min;
	j = 0;
	while (i < max)
	{
		(*range)[j] = i;
		i++;
		j++;
	}
	return (max - min);
}
