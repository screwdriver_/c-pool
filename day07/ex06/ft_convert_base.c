/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_base.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/08 16:00:52 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/10 12:33:02 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

#include "../test.c"

unsigned int	ft_check_base(char *base)
{
	int length;
	int i;
	int j;

	length = 0;
	while (base[length])
	{
		if (base[length] == '-' || base[length] == '+')
			return (0);
		length++;
	}
	if (length <= 1)
		return (0);
	i = -1;
	j = -1;
	while (i++ < length)
	{
		while (j++ < length)
		{
			if (i != j && base[i] == base[j])
				return (0);
		}
	}
	return (length);
}

int				ft_to_integer(char c, char *base)
{
	unsigned int i;

	i = 0;
	while (base[i])
	{
		if (base[i] == c)
			return (i);
		i++;
	}
	return (-1);
}

int				ft_atoi_base(char *nbr, char *base)
{
	int length;
	int neg;
	int n;
	int i;

	if (!(length = ft_check_base(base)))
		return (0);
	n = 0;
	while (*nbr && *nbr < 32)
		nbr++;
	if (!(*nbr))
		return (0);
	neg = (*nbr == '-' ? -1 : 1);
	if (*nbr == '-' || *nbr == '+')
		nbr++;
	while (*nbr)
	{
		i = ft_to_integer(*nbr, base);
		if (i == -1)
			return (0);
		n *= length;
		n += i;
		nbr++;
	}
	return (n * neg);
}

void			ft_putnbr_to_str(char *str, int nbr, char *base)
{
	int		length;
	long	n;
	unsigned int i;

	length = ft_check_base(base);
	if (length == 0)
		return ;
	n = nbr;
	if (n < 0)
	{
		*str = '-';
		n = -n;
	}
	if (n > 9)
		ft_putnbr_to_str(str, n / length, base);
	i = 0;
	while (str[i])
		i++;
	str[i] = base[n % length];
}

char			*ft_convert_base(char *nbr, char *base_from, char *base_to)
{
	char			*str;
	unsigned int	i;

	str = (char*)malloc(32 + 1);
	i = 0;
	while (i < 32 + 1)
	{
		str[i] = '\0';
		i++;
	}
	ft_putnbr_to_str(str, ft_atoi_base(nbr, base_from), base_to);
	return (str);
}

int			main(void)
{
	ft_putstr(ft_convert_base("236496", "0123456789", "1ZGPJvQ"));
}
