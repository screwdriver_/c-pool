/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_concat_params.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/08 09:05:17 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/10 11:49:21 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>

unsigned int	ft_strlen(char *str)
{
	unsigned int i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

char			*ft_stralloc(int argc, char **argv)
{
	int				i;
	unsigned int	total_size;

	i = 0;
	total_size = 0;
	while (i < argc)
	{
		total_size += ft_strlen(argv[i]) + 1;
		i++;
	}
	return (char*)malloc(total_size + 1);
}

char			*ft_concat_params(int argc, char **argv)
{
	int				i;
	int				j;
	unsigned int	k;
	char			*str;

	str = ft_stralloc(argc, argv);
	if (!str)
		return (str);
	i = 0;
	k = 0;
	while (i++ < argc - 1)
	{
		j = 0;
		while (argv[i][j])
		{
			str[k] = argv[i][j];
			j++;
			k++;
		}
		if (i < argc - 1)
			str[k++] = '\n';
	}
	str[k++] = '\0';
	return (str);
}
