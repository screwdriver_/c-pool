/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tail.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/15 12:37:14 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/23 13:47:08 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_TAIL_H
# define FT_TAIL_H

# define ABS(Value)	(Value < 0 ? -Value : Value)

# include <errno.h>
# include <fcntl.h>
# include <unistd.h>
# include <stdio.h>
# include <string.h>
# include <sys/stat.h>
# include <sys/types.h>

void	ft_read(char *file, int fd, int number);

void	ft_putchar(char c, int fd);
void	ft_putstr(char *str, int fd);
int		tail_atoi(char *str);
int		ft_strequals(char *s1, char *s2);

#endif
