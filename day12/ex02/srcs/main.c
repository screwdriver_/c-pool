/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/15 00:04:56 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/23 13:50:57 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_tail.h"

void	ft_puterror(char *file)
{
	ft_putstr("tail: ", 2);
	ft_putstr(file, 2);
	ft_putstr(": ", 2);
	ft_putstr("No such file or directory", 2);
	ft_putchar('\n', 2);
}

void	ft_putfilename(char *file)
{
	ft_putstr("==> ", 1);
	ft_putstr(file, 1);
	ft_putstr(" <==\n", 1);
}

void	foreach(int ac, char **av, int i, int number)
{
	int		begin;
	int		fd;

	begin = i;
	while (i < ac)
	{
		errno = 0;
		fd = open(av[i], O_RDONLY);
		if (errno != 0)
		{
			ft_puterror(av[i]);
			i++;
			continue ;
		}
		else if (ac - begin > 1)
		{
			if (i != begin)
				ft_putchar('\n', 1);
			ft_putfilename(av[i]);
		}
		ft_read(av[i], fd, number);
		i++;
	}
}

int		main(int ac, char **av)
{
	int i;
	int number;

	i = 1;
	number = 0;
	if (ac == 1)
		return (0);
	if (ac >= 3 && ft_strequals(av[i], "-c"))
	{
		number = tail_atoi(av[i + 1]);
		i += 2;
	}
	foreach(ac, av, i, number);
	return (0);
}
