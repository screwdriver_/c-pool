/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tail.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/23 12:17:33 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/23 13:47:26 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_tail.h"

void	ft_positive_read(int fd, int number)
{
	char	buffer;
	int		i;

	i = 0;
	while (i < number - 1)
	{
		read(fd, &buffer, 1);
		i++;
	}
	while (read(fd, &buffer, 1))
	{
		ft_putchar(buffer, 1);
		i++;
	}
	close(fd);
}

void	ft_negative_read(char *file, int fd, int number)
{
	int		length;
	char	buffer;
	int		i;

	length = 0;
	while (read(fd, &buffer, 1))
		length++;
	close(fd);
	fd = open(file, O_RDONLY);
	i = 0;
	while (i < length)
	{
		read(fd, &buffer, 1);
		if (i >= length - ABS(number))
			ft_putchar(buffer, 1);
		i++;
	}
}

void	ft_read(char *file, int fd, int number)
{
	errno = 0;
	if (number == 0)
		return ;
	if (number > 0)
		ft_positive_read(fd, number);
	else
		ft_negative_read(file, fd, number);
}
