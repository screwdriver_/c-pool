/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/14 22:21:12 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/22 12:07:50 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

void	ft_putchar(char c, int fd)
{
	write(fd, &c, 1);
}

void	ft_putstr(char *str, int fd)
{
	while (*str)
	{
		ft_putchar(*str, fd);
		str++;
	}
}

void	ft_puterror(char *file)
{
	ft_putstr("cat: ", 2);
	ft_putstr(file, 2);
	ft_putstr(": ", 2);
	ft_putstr("No such file or directory", 2);
	ft_putchar('\n', 2);
}

void	ft_read(int fd)
{
	char buffer;

	if (fd < 0)
		return ;
	while (read(fd, &buffer, sizeof(buffer)))
		ft_putchar(buffer, 1);
	close(fd);
}

int		main(int ac, char **av)
{
	int i;
	int fd;

	i = 1;
	while (i < ac)
	{
		errno = 0;
		if (av[i][0] == '-' && av[i][1] == '\0')
			ft_read(0);
		fd = open(av[i], O_RDONLY);
		if (errno == ENOENT)
		{
			ft_puterror(av[i]);
			errno = 0;
		}
		else
			ft_read(fd);
		i++;
	}
	return (0);
}
