/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   util.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/15 17:50:41 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/17 14:52:32 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_hexdump.h"

void	ft_putchar(char c, int fd)
{
	write(fd, &c, 1);
}

void	ft_putstr(char *str, int fd)
{
	while (*str)
	{
		ft_putchar(*str, fd);
		str++;
	}
}

long	ft_pow(long n, long pow)
{
	if (pow == 0)
		return (1);
	if (pow == 1)
		return (n);
	return (n * ft_pow(n, pow - 1));
}

void	ft_puthexa(unsigned int nbr, int offset)
{
	static char	*hexa = "0123456789abcdef";
	long		i;
	long		j;

	if (offset > 0)
	{
		i = 1;
		while (ft_pow(16, i) <= nbr)
			i++;
		j = offset - i;
		i = 0;
		while (i < j)
		{
			ft_putchar('0', 1);
			i++;
		}
	}
	if (nbr > 15)
		ft_puthexa(nbr / 16, 0);
	ft_putchar(hexa[nbr % 16], 1);
}

int		ft_strncmp(char *s1, char *s2, unsigned long n)
{
	unsigned long i;

	i = 0;
	while (*s1 && (*s1 == *s2) && i < n)
	{
		s1++;
		s2++;
		i++;
	}
	if (*s1 < *s2)
		return (-1);
	else if (*s1 > *s2)
		return (1);
	else
		return (0);
}
