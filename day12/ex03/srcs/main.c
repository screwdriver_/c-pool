/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/15 17:52:58 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/23 14:05:57 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_hexdump.h"

int		cmpbuffers(char *b1, char *b2)
{
	unsigned char i;

	i = 0;
	while (i < 16)
	{
		if (b1[i] != b2[i])
			return (0);
		i++;
	}
	return (1);
}

void	ft_hexdump(char *buffer, unsigned int size, int c)
{
	char			*previous;
	unsigned int	i;
	char			star;

	previous = 0;
	i = 0;
	star = 0;
	while (i < size)
	{
		if (previous && cmpbuffers(buffer + i, previous))
		{
			if (!star)
				ft_putstr("*\n", 1);
		}
		else
			ft_putdump(buffer + i, i, (i > size - 16 ? size % 16 : 16), c);
		star = previous && cmpbuffers(buffer + i, previous);
		previous = buffer + i;
		i += (i > size - 16 ? size % 16 : 16);
	}
	ft_putdump(buffer + i, i, 0, c);
	free(buffer);
}

void	ft_hexdump_file(char *file, int c)
{
	int				fd;
	char			buffer;
	char			*buff;
	unsigned int	size;

	errno = 0;
	fd = open(file, O_RDONLY);
	if (errno == ENOENT)
	{
		ft_puterror(file);
		errno = 0;
		return ;
	}
	size = 0;
	while (read(fd, &buffer, 1))
		size++;
	close(fd);
	fd = open(file, O_RDONLY);
	buff = (char*)malloc(size);
	read(fd, buff, size);
	close(fd);
	ft_hexdump(buff, size, c);
}

void	ft_hexdump_stdin(int c)
{
	char			*buffer;
	unsigned int	i;
	unsigned int	length;

	buffer = new_buffer();
	i = 0;
	while ((length = read(0, buffer + (i % 16), 16 - (i % 16))))
	{
		if (i > 0 && i % 16 == 0)
		{
			ft_putdump(buffer, i, 16, c);
			free(buffer);
			buffer = new_buffer();
		}
		i += length;
	}
	free(buffer);
}

int		main(int ac, char **av)
{
	int i;
	int c;

	av++;
	ac--;
	c = 0;
	if (ac >= 1 && ft_strncmp(*av, "-C", 3) == 0)
	{
		av++;
		ac--;
		c = 1;
	}
	if (ac == 0)
	{
		ft_hexdump_stdin(c);
		return (0);
	}
	i = 0;
	while (i < ac)
	{
		ft_hexdump_file(av[i], c);
		i++;
	}
	return (0);
}
