/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hexdump.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/15 23:25:22 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/23 14:19:57 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_hexdump.h"

char	*new_buffer(void)
{
	char			*buffer;
	unsigned int	i;

	buffer = (char*)malloc(16);
	i = 0;
	while (i < 16)
	{
		buffer[i] = '\0';
		i++;
	}
	return (buffer);
}

void	ft_puterror(char *file)
{
	ft_putstr("hexdump: ", 2);
	ft_putstr(file, 2);
	ft_putstr(": ", 2);
	ft_putstr("No such file or directory", 2);
	ft_putchar('\n', 2);
	ft_putstr("hexdump: ", 2);
	ft_putstr(file, 2);
	ft_putstr(": ", 2);
	ft_putstr("Bad file descriptor", 2);
	ft_putchar('\n', 2);
}

void	ft_putbuffer(char *buffer, int size)
{
	int i;

	ft_putstr(" |", 1);
	i = 0;
	while (i < size)
	{
		if (buffer[i] >= 32 && buffer[i] != 127)
			ft_putchar(buffer[i], 1);
		else
			ft_putchar('.', 1);
		i++;
	}
	ft_putchar('|', 1);
}

void	ft_putbody(char *buffer, int size, int c)
{
	int i;
	int end;

	i = 0;
	end = 0;
	ft_putchar(' ', 1);
	if (c)
		ft_putchar(' ', 1);
	while (i < 16)
	{
		if (i < size)
			ft_puthexa(((unsigned int)buffer[i])
				<< (3 * 8) >> (3 * 8), 2);
		else
			ft_putstr("  ", 1);
		if (i == 7 && c)
			ft_putchar(' ', 1);
		if (i < 15 || c)
			ft_putchar(' ', 1);
		i++;
	}
	if (c)
		ft_putbuffer(buffer, size);
}

void	ft_putdump(char *buffer, unsigned long pos, int size, int c)
{
	ft_puthexa(pos, (c ? 8 : 7));
	if (size > 0)
		ft_putbody(buffer, size, c);
	ft_putchar('\n', 1);
}
