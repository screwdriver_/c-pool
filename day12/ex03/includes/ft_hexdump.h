/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hexdump.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/15 17:51:13 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/23 14:06:44 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_HEXDUMP_H
# define FT_HEXDUMP_H

# include <errno.h>
# include <fcntl.h>
# include <stdlib.h>
# include <string.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <unistd.h>

void	ft_putchar(char c, int fd);
void	ft_putstr(char *str, int fd);
int		ft_strncmp(char *s1, char *s2, unsigned long n);
void	ft_puthexa(unsigned int nbr, int offset);

char	*new_buffer(void);
void	ft_puterror(char *file);
void	ft_putdump(char *buffer, unsigned long pos, int size, int c);

#endif
