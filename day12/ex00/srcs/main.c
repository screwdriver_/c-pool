/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/14 21:24:48 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/18 18:17:39 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

void	ft_putchar(char c, int fd)
{
	write(fd, &c, 1);
}

void	ft_putstr(char *str, int fd)
{
	while (*str)
	{
		ft_putchar(*str, fd);
		str++;
	}
}

void	ft_putfilecontent(char *path)
{
	int		fd;
	char	buffer;

	fd = open(path, O_RDONLY);
	if (fd < 0)
		return ;
	while (read(fd, &buffer, sizeof(buffer)))
		ft_putchar(buffer, 1);
	close(fd);
}

int		main(int ac, char **av)
{
	if (ac == 1)
	{
		ft_putstr("File name missing.\n", 2);
		return (0);
	}
	if (ac > 2)
	{
		ft_putstr("Too many arguments.\n", 2);
		return (0);
	}
	ft_putfilecontent(av[1]);
}
