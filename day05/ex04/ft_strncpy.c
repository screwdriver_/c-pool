/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/04 21:49:19 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/07 16:10:56 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

char	*ft_strncpy(char *dest, char *src, unsigned int n)
{
	unsigned int	i;
	int				end;

	i = 0;
	end = 0;
	while (i < n)
	{
		dest[i] = src[i];
		if (!src[i] && end)
			end = 1;
		if (end)
			dest[i] = '\0';
		i++;
	}
	return (dest);
}
