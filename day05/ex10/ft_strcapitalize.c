/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/05 18:21:11 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/06 16:06:53 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

char	*ft_strcapitalize(char *str)
{
	int i;
	int begin;

	i = 0;
	begin = 1;
	while (str[i])
	{
		if (str[i] >= 'a' && str[i] <= 'z')
		{
			str[i] += (begin ? 'A' - 'a' : 0);
			begin = 0;
		}
		else if (str[i] >= 'A' && str[i] <= 'Z')
		{
			str[i] += (!begin ? 'a' - 'A' : 0);
			begin = 0;
		}
		else if (str[i] >= '0' && str[i] <= '9')
			begin = 0;
		else
			begin = 1;
		i++;
	}
	return (str);
}
