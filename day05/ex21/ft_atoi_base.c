/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/06 10:29:41 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/07 01:02:36 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int	ft_check_base(char *base)
{
	int length;
	int i;
	int j;

	length = 0;
	while (base[length])
	{
		if (base[length] == '-' || base[length] == '+')
			return (0);
		length++;
	}
	if (length <= 1)
		return (0);
	i = -1;
	j = -1;
	while (i++ < length)
	{
		while (j++ < length)
		{
			if (i != j && base[i] == base[j])
				return (0);
		}
	}
	return (length);
}

int	ft_str_contains(char *str, char c)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (str[i] == c)
			return (1);
		i++;
	}
	return (0);
}

int	ft_check_str(char *str, char *base)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (!ft_str_contains(base, str[i]))
			return (0);
		i++;
	}
	return (1);
}

int	ft_get_number(char c, char *base)
{
	int i;

	i = 0;
	while (base[i])
	{
		if (base[i] == c)
			return (i);
		i++;
	}
	return (0);
}

int	ft_atoi_base(char *str, char *base)
{
	int length;
	int neg;
	int n;
	int nbr;

	length = ft_check_base(base);
	if (length == 0)
		return (0);
	neg = 0;
	n = 0;
	nbr = 0;
	while ((str[n] <= 32) && str[n] != '\0')
		n++;
	if ((neg = (str[n] == '-')) || str[n] == '+')
		n++;
	if (!ft_check_str(str + n, base))
		return (0);
	while (str[n])
	{
		nbr *= length;
		nbr += ft_get_number(str[n], base);
		n++;
	}
	return (neg ? -nbr : nbr);
}
