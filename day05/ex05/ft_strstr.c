/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/04 22:29:36 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/06 15:11:51 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

char	*ft_strstr(char *str, char *to_find)
{
	int length;
	int length2;
	int i;
	int j;

	length = ft_strlen(str);
	length2 = ft_strlen(to_find);
	if (length2 == 0)
		return (str);
	i = 0;
	while (i < length)
	{
		j = 0;
		while (j < length2 && str[i + j] == to_find[j])
		{
			if (i + j >= length)
				return (NULL);
			if (j == length2 - 1)
				return (str + i);
			j++;
		}
		i++;
	}
	return (NULL);
}
