/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/04 21:37:24 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/06 12:55:36 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

char	*ft_strcpy(char *dest, char *src)
{
	int length;
	int n;

	length = 0;
	n = 0;
	while (src[length])
		length++;
	while (n < length)
	{
		dest[n] = src[n];
		n++;
	}
	dest[length] = '\0';
	return (dest);
}
