/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_memory.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/06 11:58:17 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/07 01:47:00 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c);

void	ft_putstr_hexa(unsigned long nbr)
{
	char *base;

	base = "0123456789abcdef";
	if (nbr > 9)
		ft_putstr_hexa(nbr / 16);
	ft_putchar(base[nbr % 16]);
}

void	ft_puthexa(void *addr, unsigned int size, int spacing)
{
	unsigned int i;
	unsigned int chars;

	i = -1;
	chars = 0;
	while (i++ < size)
	{
		if (*((char*)addr + i) < 16)
			ft_putchar('0');
		ft_putstr_hexa(*((char*)addr + i));
		chars += 2;
		if (spacing && i % 2 == 1 && i < size - 1)
		{
			ft_putchar(' ');
			chars++;
		}
	}
	if (spacing)
	{
		while (chars < 40)
		{
			ft_putchar(' ');
			chars++;
		}
	}
}

void	ft_putchars(char *c, unsigned int size)
{
	unsigned int i;

	i = 0;
	while (i < size)
	{
		ft_putchar((c[i] >= 32 ? c[i] : '.'));
		i++;
	}
}

void	*ft_print_memory(void *addr, unsigned int size)
{
	unsigned int i;

	if (size == 0)
		return (addr);
	i = 0;
	while (i < size)
	{
		ft_putstr_hexa((unsigned long)addr);
		ft_putchar(':');
		ft_putchar(' ');
		ft_puthexa(addr + i, (size - i > 16 ? 16 : size - i), 1);
		ft_putchar(' ');
		ft_putchars(addr + i, (size - i > 16 ? 16 : size - i));
		ft_putchar('\n');
		i += 16;
	}
	return (addr);
}
