/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/05 23:11:57 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/07 15:31:59 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c);

int		ft_check_base(char *base)
{
	int length;
	int i;
	int j;

	length = 0;
	while (base[length])
	{
		if (base[length] == '-' || base[length] == '+')
			return (0);
		length++;
	}
	if (length <= 1)
		return (0);
	i = -1;
	j = -1;
	while (i++ < length)
	{
		while (j++ < length)
		{
			if (i != j && base[i] == base[j])
				return (0);
		}
	}
	return (length);
}

void	ft_putnbr_base(int nbr, char *base)
{
	int		length;
	long	n;

	length = ft_check_base(base);
	if (length == 0)
		return ;
	n = nbr;
	if (n < 0)
	{
		ft_putchar('-');
		n = -n;
	}
	if (n > 9)
		ft_putnbr_base(n / length, base);
	ft_putchar(base[n % length]);
}
