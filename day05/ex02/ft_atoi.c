/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/04 21:33:15 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/06 12:46:45 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int	ft_atoi(char *str)
{
	int neg;
	int n;
	int nbr;

	neg = 0;
	n = 0;
	nbr = 0;
	if (str[0] == '\0')
		return (0);
	while (str[n] == ' ' || str[n] == '\a' || str[n] == '\b'
			|| str[n] == '\t' || str[n] == '\n' || str[n] == '\v'
			|| str[n] == '\f' || str[n] == '\r')
		n++;
	if (str[n] == '-')
		neg = 1;
	if (str[n] == '-' || str[n] == '+')
		n++;
	while (str[n] >= '0' && str[n] <= '9')
	{
		nbr *= 10;
		nbr += str[n] - '0';
		n++;
	}
	return (neg ? -nbr : nbr);
}
