/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/20 17:26:47 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/22 20:02:57 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/bsq.h"

char	*grow(char *buffer, int size, int new_size)
{
	char	*buff;
	int		i;

	if (!(buff = (char*)malloc(new_size)))
		exit(0);
	i = 0;
	while (i < size)
	{
		buff[i] = buffer[i];
		i++;
	}
	free(buffer);
	return (buff);
}

char	*read_line(int fd)
{
	int		size;
	char	*buffer;

	size = 1;
	if (!(buffer = (char*)malloc(size)))
		exit(0);
	*buffer = '\0';
	errno = 0;
	while (read(fd, buffer + size - 1, 1))
	{
		if (errno != 0)
			return (0);
		if (buffer[size - 1] == '\n')
			break ;
		if (!(buffer = grow(buffer, size, size + 1)))
			exit(0);
		size++;
	}
	return (buffer);
}
