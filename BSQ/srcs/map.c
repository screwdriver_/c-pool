/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/20 14:30:11 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/22 21:28:17 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/bsq.h"

int		is_possible(t_map map, t_pos pos, int size)
{
	int		line_length;
	t_pos	i;

	line_length = ft_strlen(map.content[0]);
	if ((pos.x + size > line_length) || (pos.y + size > map.lines))
		return (0);
	i.y = pos.y;
	while (i.y < pos.y + size)
	{
		i.x = pos.x;
		while (i.x < pos.x + size)
		{
			if (map.content[i.y][i.x] == map.obstacle)
				return (0);
			i.x++;
		}
		i.y++;
	}
	return (1);
}

void	draw_square(t_map map, t_pos pos, int size)
{
	t_pos i;

	i.y = pos.y;
	while (i.y < pos.y + size)
	{
		i.x = pos.x;
		while (i.x < pos.x + size)
		{
			map.content[i.y][i.x] = map.full;
			i.x++;
		}
		i.y++;
	}
}

void	check_square(t_map map, t_pos *pos, int *best, t_pos i)
{
	while (1)
	{
		if (is_possible(map, i, *best + 1))
		{
			*pos = i;
			(*best)++;
		}
		else
			break ;
	}
}

t_map	find_square(t_map map)
{
	int		line_length;
	t_pos	i;
	t_pos	pos;
	int		best;

	line_length = ft_strlen(map.content[0]);
	i.y = -1;
	best = 0;
	while (i.y++ < map.lines)
	{
		i.x = -1;
		while (i.x++ < line_length)
			check_square(map, &pos, &best, i);
	}
	draw_square(map, pos, best);
	return (map);
}

void	print_map(t_map map)
{
	int i;
	int j;

	i = 0;
	while (map.content[i])
	{
		j = 0;
		while (map.content[i][j])
		{
			ft_putchar(map.content[i][j]);
			j++;
		}
		ft_putchar('\n');
		i++;
	}
}
