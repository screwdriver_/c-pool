/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/22 21:35:46 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/22 21:37:36 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/bsq.h"

void	parse_head(int fd, t_map *map)
{
	char	*head;
	int		length;

	head = read_line(fd);
	if (!head || !*head || *head == '\n')
	{
		map->lines = 0;
		return ;
	}
	if ((length = ft_strlen(head)) < 4)
	{
		map->lines = 0;
		return ;
	}
	map->lines = ft_atoi(head, length - 3);
	map->empty = *(head + length - 3);
	map->obstacle = *(head + length - 2);
	map->full = *(head + length - 1);
}

void	parse_body(int fd, t_map *map)
{
	int		i;

	if (!(map->content = (char**)malloc(sizeof(char*) * (map->lines + 1))))
		exit(0);
	i = 0;
	while (i < map->lines)
	{
		map->content[i] = read_line(fd);
		replace_newlines(map->content[i]);
		i++;
	}
	map->content[i] = 0;
	if (*read_line(fd) == '\n')
		map->lines = 0;
}

t_map	parse_map(int fd)
{
	t_map map;

	parse_head(fd, &map);
	if (map.lines > 0)
		parse_body(fd, &map);
	if (map.content == 0)
		map.lines = 0;
	return (map);
}

t_map	parse_map_file(char *file)
{
	int		fd;
	t_map	map;

	errno = 0;
	fd = open(file, O_RDONLY);
	if (errno != 0)
	{
		errno = 0;
		map.lines = 0;
		return (map);
	}
	map = parse_map(fd);
	close(fd);
	return (map);
}
