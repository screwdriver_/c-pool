/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/20 12:14:41 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/22 21:37:15 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/bsq.h"

void	handle_map(t_map map)
{
	if (check_map(map))
	{
		map = find_square(map);
		print_map(map);
	}
	else
		ft_putstr("map error\n");
}

int		main(int argc, char **argv)
{
	t_map	map;
	int		i;

	argc--;
	argv++;
	if (argc == 0)
	{
		map = parse_map(0);
		handle_map(map);
	}
	else
	{
		i = 0;
		while (i < argc)
		{
			map = parse_map_file(argv[i]);
			handle_map(map);
			if (i < argc - 1)
				ft_putchar('\n');
			i++;
		}
	}
	return (0);
}
