/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/19 15:45:25 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/19 19:23:55 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/colle_02.h"

char	*grow(char *buffer, int size, int new_size)
{
	char *buff;
	int i;

	buff = (char*)malloc(new_size);
	i = 0;
	while (i < size)
	{
		buff[i] = buffer[i];
		i++;
	}
	free(buffer);
	return (buff);
}

char	read_next(void)
{
	char buffer;

	read(0, &buffer, 1);
	return (buffer);
}

char	*read_line(int *size)
{
	char *buffer;
	int i;

	buffer = (char*)malloc(1);
	i = 0;
	while ((buffer[i] = next_char()) != '\n')
	{
		buffer = grow(buffer, i + 1, i + 2);
		i++;
	}
	*size = i;
	return (buffer);
}

char	**read_tab(void)
{
	char	**tab;
	int		size;
	char	*l;
	int		i;

	l = read_line(&size);
	tab = (char**)malloc(sizeof(char*) * (size + 1));
	i = 0;
	while (i < size)
	{
		tab[i] = read_line();
		i++;
	}
	return (tab);
}
