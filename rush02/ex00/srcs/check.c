/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/19 14:47:21 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/19 15:32:38 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/colle_02.h"

int		is_00(char c, char prev)
{
	if (c == 'o' && (prev == 0 || prev == '-'))
		return (1);
	if (c == '-' && (prev == 'o' || prev == '-'))
		return (1);
	if (c == '\0' && (prev == 'o'))
		return (1);
	return (0);
}

int		is_01(char c, char prev)
{
	if (c == 'o' && (prev == 0 || prev == '-'))
		return (1);
	if (c == '-' && (prev == 'o' || prev == '-'))
		return (1);
	if (c == '\0' && (prev == 'o'))
		return (1);
	return (0);
}

int		check_top(int *width)
{
	char c;
	char prev;
	unsigned int i;

	i = 0;
	prev = '\0';
	while (read(0, c, 1))
	{
		// TODO
		i++;
	}
	if (i == 0)
		return (-1);
	prev = c;
}

int		check_middle(int id, int *height)
{
	// TODO
}

int		check(int *width, int *height)
{
	int i;
	int j;
	unsigned int width;
	unsigned int height;

	i = check_top(&width);
	height = 1;
	if (check_middle(i, &height) == 0)
		return (0);
	return (i);
}
