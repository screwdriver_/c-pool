/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check0.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/19 15:37:11 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/19 20:01:12 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/colle_02.h"

int	check_top(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (i == 0 && !str[i] == 'o')
			return (0);
		else if (str[i] == )
		i++;
	}
	if (str[i - 1] != 'o')
		return (0);
	return (i);
}

int	check_middle(char *str, unsigned int width)
{
	// TODO
}

int	check_bottom(char *str, unsigned int width)
{
	// TODO
}

int	is_rush00(char **tab, int *width, int *height)
{
	int i;

	*width = 0;
	*height = ft_tabsize(tab);
	i = 0;
	while (i < *height)
	{
		if (*height == 0)
			*width = check_top(str);
		else if (i == *height - 1)
		{
			if (!check_bottom(tab[i], width))
				return (0);
		}
		else
		{
			if (!check_middle(tab[i], width))
				return (0);
		}
		if (width == 0 || height == 0)
			return (0);
		i++;
	}
	return (1);
}
