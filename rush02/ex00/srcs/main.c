/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/18 14:28:39 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/19 19:54:03 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/colle_02.h"

void	print_result(int *first, int id, int width, int height)
{
	if (!*first)
		ft_putstr(" || ");
	else
		first = 0;
	ft_putchar('[');
	ft_putstr("colle-0");
	ft_putnbr(id);
	ft_putstr("] [");
	ft_putnbr(width);
	ft_putstr("] [");
	ft_putnbr(height);
	ft_putchar(']');
}

int		check_all()
{
	char **tab;
	int first;
	int width;
	int height;

	tab = read_tab();
	first = 1;
	width = 0;
	height = 0;
	if (is_rush00(tab, &width, &height))
		print_result(&first, 0, width, height);
	if (is_rush01(tab, &width, &height))
		print_result(&first, 1, width, height);
	if (is_rush02(tab, &width, &height))
		print_result(&first, 2, width, height);
	if (is_rush03(tab, &width, &height))
		print_result(&first, 3, width, height);
	if (is_rush04(tab, &width, &height))
		print_result(&first, 4, width, height);
	return (!first);
}

int	main(void)
{
	if (!check_all())
		ft_putstr("aucune");
	ft_putchar('\n');
	return (0);
}
