/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colle_02.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/18 14:26:00 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/19 19:35:47 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COLLE_02_H
# define COLLE_02_H

# include <stdlib.h>
# include <unistd.h>

int		is_rush00(char **tab, int *width, int *height);
int		is_rush01(char **tab, int *width, int *height);
int		is_rush02(char **tab, int *width, int *height);
int		is_rush03(char **tab, int *width, int *height);
int		is_rush04(char **tab, int *width, int *height);

void	ft_putchar(char c);
void	ft_putstr(char *str);
void	ft_putnbr(unsigned int n);
int		ft_tabsize(char **tab);
int		ft_strlen(char *str);

#endif
