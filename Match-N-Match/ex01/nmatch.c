/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nmatch.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/11 01:03:19 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/12 20:00:40 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	nmatch(char *s1, char *s2)
{
	if (*s1 == '\0')
	{
		if (*s2 == '\0')
			return (0);
		else if (*s2 == '*')
			return (1);
		else
			return (0);
	}
	else
	{
		if (*s2 == '\0')
			return (0);
		else if (*s1 == *s2)
			return (nmatch(s1 + 1, s2 + 1));
		else if (*s2 == '*')
			return (nmatch(s1 + 1, s2) + nmatch(s1, s2 + 1));
	}
	return (0);
}
