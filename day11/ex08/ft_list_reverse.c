/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_reverse.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/13 16:10:47 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/13 21:25:25 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

void	ft_list_reverse(t_list **begin_list)
{
	t_list *current;
	t_list *next;
	t_list *previous;

	current = *begin_list;
	while (current)
	{
		next = current->next;
		current.next = previous;
		previous = current;
		current = next;
	}
	*begin_list = previous;
}
