/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_merge.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/13 23:32:02 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/13 23:35:26 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_list_merge(t_list **begin_list1, t_list *begin_list2)
{
	t_list *i;

	i = *begin_list1;
	while (i->next)
		i = i->next;
	i->next = begin_list2;
}
