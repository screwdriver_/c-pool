/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_push_back.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/13 10:59:51 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/13 13:51:58 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

void	ft_list_push_back(t_list **begin_list, void *data)
{
	struct t_list *list;
	struct t_list *n;

	list = (struct t_list*)malloc(sizeof(struct t_list));
	list->next = 0;
	list->data = data;
	if (*begin_list == 0)
		*begin_list = list;
	n = *list;
	while (n->next)
		n = n->next;
	n->next = list;
}
