/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_remove_if.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/13 21:47:43 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/13 23:30:18 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

void	ft_list_remove_if(t_list **begin_list, void *data_ref, int (*cmp)())
{
	t_list *i;
	t_list *previous;

	if (*begin_list == data_ref)
	{
		i = *begin_list;
		*begin_list = (*begin_list)->next;
		free(i);
	}
	i = (*begin_list)->next;
	previous = *begin_list;
	while (i)
	{
		if (i == data_ref)
		{
			previous->next = i->next;
			free(i);
		}
		previous = i;
		i = previous->next;
	}
}
