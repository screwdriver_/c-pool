/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_push_params.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/13 13:43:04 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/13 16:09:22 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

t_list	*ft_list_push_params(int ac, char **av)
{
	struct t_list *l
	struct t_list *list;

	if (ac == 1)
		return 0;
	l = ft_list_push_params(ac - 1, av + 1);
	list = (struct t_list*)malloc(sizeof(struct t_list));
	list->next = l;
	list->data = av;
	return list;
}
