/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_elem.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/13 10:53:17 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/13 13:51:25 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

t_list	*ft_create_elem(void *data)
{
	struct t_list	*list;

	list = (struct t_list*)malloc(sizeof(struct t_list));
	list->next = 0;
	list->data = data;
	return (list);
}
