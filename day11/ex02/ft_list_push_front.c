/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_push_front.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/13 13:23:48 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/13 13:35:26 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

void	ft_list_push_front(t_list **begin_list, void *data)
{
	struct t_list *list;
	struct t_list *n;

	list = (struct t_list*)malloc(sizeof(struct t_list));
	list->next = *begin_list;
	list->data = data;
	*begin_list = list;
}
