/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_foreach_if.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/13 21:14:02 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/13 21:24:32 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

void	ft_list_foreach_if(t_list *begin_list, void (*f)(void *),
	void *data_ref, int (*cmp)(void *, void *))
{
	if (begin_list == 0)
		return ;
	if (cmp(begin_list->data, data_ref))
		f(begin_list->data);
	ft_list_foreach_if(begin_list->next);
}
