/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_sort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/12 11:16:47 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/14 18:10:41 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_is_sort(int *tab, int length, int (*f)(int, int))
{
	int i;
	int sign;

	if (length <= 1)
		return (1);
	i = 0;
	sign = 0;
	while (i < length - 1)
	{
		if (sign == 0)
			sign = f(tab[i], tab[i + 1]);
		if (sign == 0)
		{
			i++;
			continue ;
		}
		if (sign > 0 && f(tab[i], tab[i + 1]) < 0)
			return (0);
		if (sign < 0 && f(tab[i], tab[i + 1]) > 0)
			return (0);
		i++;
	}
	return (1);
}
