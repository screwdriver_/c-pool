/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   do_op.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/12 11:34:35 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/12 21:23:18 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DO_OP_H
# define DO_OP_H

void					ft_putchar(char c);
void					ft_putstr(char *str);
void					ft_putnbr(int nbr);
int						ft_atoi(char *str);

struct					s_opp
{
	char *op;
	void (*func)(int, int);
};

typedef struct s_opp	t_opp;

void					ft_add(int val1, int val2);
void					ft_sub(int val1, int val2);
void					ft_mul(int val1, int val2);
void					ft_div(int val1, int val2);
void					ft_mod(int val1, int val2);
void					ft_usage(int val1, int val2);

#endif
