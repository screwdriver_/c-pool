/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   util.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/12 11:30:54 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/13 15:57:15 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	while (*str)
	{
		ft_putchar(*str);
		str++;
	}
}

void	ft_putnbr(int nbr)
{
	if (nbr < 0)
	{
		if (nbr == -2147483648)
		{
			ft_putchar('-');
			ft_putchar('2');
			nbr = 147483648;
		}
		else
		{
			ft_putchar('-');
			nbr = -nbr;
		}
	}
	if (nbr > 9)
		ft_putnbr(nbr / 10);
	ft_putchar('0' + (nbr % 10));
}

int		ft_atoi(char *str)
{
	unsigned int	i;
	int				n;
	int				neg;

	i = 0;
	neg = 1;
	while (str[i] <= 32)
		i++;
	if (str[i] == '-')
		neg = -1;
	if (str[i] == '+' || str[i] == '-')
		i++;
	n = 0;
	while (str[i] >= '0' && str[i] <= '9')
	{
		n *= 10;
		n += str[i] - '0';
		i++;
	}
	return (n * neg);
}
