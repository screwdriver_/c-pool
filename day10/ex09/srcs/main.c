/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/12 11:29:41 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/12 21:28:38 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/do_op.h"
#include "../includes/ft_opp.h"

void	ft_usage(int val1, int val2)
{
	unsigned int i;

	(void)val1;
	(void)val2;
	ft_putstr("error : only [ ");
	i = 0;
	while (i < sizeof(g_opptab) / sizeof(t_opp))
	{
		if (*(g_opptab[i].op) == '\0')
		{
			i++;
			continue ;
		}
		ft_putstr(g_opptab[i].op);
		ft_putchar(' ');
		i++;
	}
	ft_putstr("] are accepted.\n");
}

int		ft_strcmp(char *s1, char *s2)
{
	while (*s1 && (*s1 == *s2))
	{
		s1++;
		s2++;
	}
	return (int)(((unsigned char)*s1) - ((unsigned char)*s2));
}

void	(*get_function(char *op))(int, int)
{
	unsigned int	i;

	i = 0;
	while (i < sizeof(g_opptab) / sizeof(t_opp))
	{
		if (ft_strcmp(g_opptab[i].op, op) == 0)
			return (g_opptab[i].func);
		i++;
	}
	return (&ft_usage);
}

void	compute(char *val1, char *op, char *val2)
{
	void	(*f)(int, int);
	int		i1;
	int		i2;

	f = get_function(op);
	if (f == 0)
		return ;
	i1 = ft_atoi(val1);
	i2 = ft_atoi(val2);
	f(i1, i2);
}

int		main(int argc, char *argv[])
{
	if (argc != 4)
		return (0);
	argv++;
	compute(argv[0], argv[1], argv[2]);
	return (0);
}
