/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   functions.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/12 12:14:07 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/12 21:16:28 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/do_op.h"

void	ft_add(int val1, int val2)
{
	ft_putnbr(val1 + val2);
	ft_putchar('\n');
}

void	ft_sub(int val1, int val2)
{
	ft_putnbr(val1 - val2);
	ft_putchar('\n');
}

void	ft_mul(int val1, int val2)
{
	ft_putnbr(val1 * val2);
	ft_putchar('\n');
}

void	ft_div(int val1, int val2)
{
	if (val2 == 0)
	{
		ft_putstr("Stop : division by zero\n");
		return ;
	}
	ft_putnbr(val1 / val2);
	ft_putchar('\n');
}

void	ft_mod(int val1, int val2)
{
	if (val2 == 0)
	{
		ft_putstr("Stop : modulo by zero\n");
		return ;
	}
	ft_putnbr(val1 % val2);
	ft_putchar('\n');
}
