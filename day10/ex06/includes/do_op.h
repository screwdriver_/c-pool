/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   do_op.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/12 11:34:35 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/12 12:55:43 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DO_OP_H
# define DO_OP_H

void	ft_putchar(char c);
void	ft_putstr(char *str);
void	ft_putnbr(int nbr);
int		ft_atoi(char *str);

int		add(int val1, int val2);
int		sub(int val1, int val2);
int		mul(int val1, int val2);
int		div(int val1, int val2);
int		mod(int val1, int val2);

#endif
