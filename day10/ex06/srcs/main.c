/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/12 11:29:41 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/14 18:16:26 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/do_op.h"

static char		g_ops[] = {'+', '-', '*', '/', '%', 0};
static int		(*g_funcs[])(int, int) =
{
	add,
	sub,
	mul,
	div,
	mod
};

int		(*get_function(char op))(int, int)
{
	unsigned int	i;

	i = 0;
	while (g_ops[i])
	{
		if (g_ops[i] == op)
			return (g_funcs[i]);
		i++;
	}
	return (0);
}

void	compute(char *val1, char *op, char *val2)
{
	int (*f)(int, int);
	int i1;
	int i2;

	f = get_function(*op);
	if (f == 0)
	{
		ft_putnbr(0);
		ft_putchar('\n');
		return ;
	}
	i1 = ft_atoi(val1);
	i2 = ft_atoi(val2);
	if (i2 == 0)
	{
		if (*op == '/')
		{
			ft_putstr("Stop : division by zero\n");
			return ;
		}
		else if (*op == '%')
		{
			ft_putstr("Stop : modulo by zero\n");
			return ;
		}
	}
	ft_putnbr(f(i1, i2));
	ft_putchar('\n');
}

int		main(int argc, char *argv[])
{
	if (argc != 4)
		return (0);
	argv++;
	compute(argv[0], argv[1], argv[2]);
	return (0);
}
