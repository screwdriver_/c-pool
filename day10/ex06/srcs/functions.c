/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   functions.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/12 12:14:07 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/12 14:37:46 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/do_op.h"

int	add(int val1, int val2)
{
	return (val1 + val2);
}

int	sub(int val1, int val2)
{
	return (val1 - val2);
}

int	mul(int val1, int val2)
{
	return (val1 * val2);
}

int	div(int val1, int val2)
{
	return (val1 / val2);
}

int	mod(int val1, int val2)
{
	return (val1 % val2);
}
