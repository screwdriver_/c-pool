/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_wordtab.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/12 14:40:07 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/13 20:37:09 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_strcmp(char *s1, char *s2)
{
	while (*s1 && (*s1 == *s2))
	{
		s1++;
		s2++;
	}
	return (int)(((unsigned char)*s1) - ((unsigned char)*s2));
}

void	ft_swap(char **a, char **b)
{
	char *tmp;

	tmp = *a;
	*a = *b;
	*b = tmp;
}

void	ft_sort_wordtab(char **tab)
{
	unsigned int	length;
	unsigned int	i;
	unsigned int	j;

	length = 0;
	while (tab[length])
		length++;
	i = 0;
	while (i < length)
	{
		j = 0;
		while (j < length)
		{
			if (i != j && ft_strcmp(tab[i], tab[j]) < 0)
				ft_swap(tab + i, tab + j);
			j++;
		}
		i++;
	}
}
