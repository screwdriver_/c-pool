/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_advanced_sort_wordtab.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/12 15:26:04 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/14 18:59:35 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_swap(char **a, char **b)
{
	char *tmp;

	tmp = *a;
	*a = *b;
	*b = tmp;
}

void	ft_advanced_sort_wordtab(char **tab, int (*cmp)(char *, char *))
{
	unsigned int	length;
	unsigned int	i;
	unsigned int	j;

	length = 0;
	while (tab[length])
		length++;
	i = 0;
	while (i < length)
	{
		j = 0;
		while (j < length)
		{
			if (i != j && cmp(tab[i], tab[j]) > 0)
				ft_swap(tab + i, tab + j);
			j++;
		}
		i++;
	}
}
