/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_map.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/12 11:05:01 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/13 14:46:31 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int	*ft_map(int *tab, int length, int (*f)(int))
{
	int *t;
	int i;

	t = (int*)malloc(sizeof(int) * length);
	i = 0;
	while (i < length)
	{
		t[i] = f(tab[i]);
		i++;
	}
	return (t);
}
