/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/03 13:49:26 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/05 12:33:16 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int	ft_iterative_factorial(int nb)
{
	int i;

	if (nb < 0)
		return (0);
	i = 1;
	while (nb > 0)
	{
		i *= nb;
		nb--;
	}
	return (i);
}
