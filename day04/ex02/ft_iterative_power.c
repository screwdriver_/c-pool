/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_power.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/03 14:10:39 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/03 15:47:59 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int	ft_iterative_power(int nb, int power)
{
	int i;
	int n;

	if (power < 0)
		return (0);
	i = 0;
	n = 1;
	while (i < power)
	{
		n *= nb;
		i++;
	}
	return (n);
}
