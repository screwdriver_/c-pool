/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/01 14:18:36 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/02 19:03:33 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c);

void	ft_print_comb(void)
{
	char i;
	char j;
	char k;

	i = '0' - 1;
	while (i <= '7')
	{
		j = i++ + 1;
		while (j <= '8')
		{
			k = j++ + 2;
			while (k <= '9')
			{
				ft_putchar(i);
				ft_putchar(j);
				ft_putchar(k++);
				if (i < '7' || j < '8' || k < '9')
				{
					ft_putchar(',');
					ft_putchar(' ');
				}
			}
		}
	}
}
