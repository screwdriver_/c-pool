/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_combn.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/02 08:42:33 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/02 15:16:03 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putnbr(int nb)
{
	if (nb < 0)
	{
		ft_putchar('-');
		nb = -nb;
	}
	if (nb > 9)
	{
		ft_putnbr(nb / 10);
	}
	ft_putchar('0' + nb % 10);
}

void	ft_printnbr(int n)
{
	if (n < 10)
	{
		ft_putchar('0');
	}
	ft_putnbr(n);
}

void	ft_print_c(int *a, int s, int n)
{
	int i;

	i = 0;
	if (n >= s)
		return ;
	while (a[n] <= 99)
	{
		while (i < s)
		{
			ft_putnbr(a[n]);
			ft_putchar(' ');
			i++;
		}
		ft_putchar(',');
		ft_putchar(' ');
		ft_print_c(a, s, n + 1);
		if (n + 1 < s)
		{
			a[n + 1] = a[n]++;
		}
	}
}

void	ft_print_combn(int n)
{
	int a[n];

	if (n == 0)
		return ;
	a[0] = -1;
	ft_print_c(a, n, 0);
}
