/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/01 21:20:31 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/02 19:21:28 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c);

void	ft_putnbr(int nb)
{
	if (nb < 0)
	{
		ft_putchar('-');
		nb = -nb;
	}
	if (nb > 9)
	{
		ft_putnbr(nb / 10);
	}
	ft_putchar('0' + nb % 10);
}

void	ft_printnbr(int n)
{
	if (n < 10)
	{
		ft_putchar('0');
	}
	ft_putnbr(n);
}

void	ft_print_comb2(void)
{
	int a;
	int b;

	a = -1;
	while (a <= 99)
	{
		b = a++ + 2;
		while (b <= 99)
		{
			ft_printnbr(a);
			ft_putchar(' ');
			ft_printnbr(b);
			if (a < 98 || b < 99)
			{
				ft_putchar(',');
				ft_putchar(' ');
			}
			b++;
		}
	}
}
