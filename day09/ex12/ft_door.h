/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_door.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/10 14:52:40 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/10 15:18:42 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_DOOR_H
# define FT_DOOR_H

# define TRUE	1
# define FALSE	0

# define OPEN	1
# define CLOSE	0

typedef ft_bool	unsigned char;

struct	s_door
{
	t_bool state;
}	t_door;

#endif
