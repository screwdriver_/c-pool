IPS=`ifconfig | grep 'inet ' | cut -c 7- | cut -d ' ' -f 1 | tr -d ' '`

if ["$IPS" == ""]; then
	echo "I am lost!"
else
	echo "$IPS"
fi
