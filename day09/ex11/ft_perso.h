/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_perso.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/10 14:43:24 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/10 15:17:58 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PERSO_H
# define FT_PERSO_H

# define SAVE_THE_WORLD 0

struct	s_perso
{
	char	*name;
	float	life;
	int		age;
	int		profession;
}		t_perso;

char	*strdup(char *str);

#endif
