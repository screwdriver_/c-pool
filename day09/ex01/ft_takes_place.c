/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_takes_place.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/09 17:43:36 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/09 18:16:13 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putnbr(int nb)
{
	if (nb == -2147483648)
	{
		ft_putchar('-');
		ft_putchar('2');
		nb = 147483648;
	}
	if (nb < 0)
	{
		ft_putchar('-');
		nb = -nb;
	}
	if (nb > 9)
		ft_putnbr(nb / 10);
	ft_putchar('0' + nb % 10);
}

void	ft_putstr(char *str)
{
	while (*str)
	{
		write(1, str, 1);
		str++;
	}
}

void	ft_to_am_pm(int hour, int *result, char *am)
{
	if (hour == 0)
	{
		*result = 12;
		*am = 1;
	}
	else if (hour == 12)
	{
		*result = 12;
		*am = 0;
	}
	else if (hour < 12)
	{
		*result = hour;
		*am = 1;
	}
	else if (hour < 24)
	{
		*result = hour - 12;
		*am = 0;
	}
}

void	ft_takes_place(int hour)
{
	int		begin;
	char	begin_am;
	int		end;
	char	end_am;

	ft_to_am_pm(hour % 24, &begin, &begin_am);
	ft_to_am_pm((hour + 1) % 24, &end, &end_am);
	ft_putstr("THE FOLLOWING TAKES PLACE BETWEEN ");
	ft_putnbr(begin);
	ft_putstr(begin_am ? ".00 A.M. AND " : ".00 P.M. AND ");
	ft_putnbr(end);
	ft_putstr(end_am ? ".00 A.M." : ".00 P.M.");
	ft_putchar('\n');
}
