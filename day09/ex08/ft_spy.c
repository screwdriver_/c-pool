/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_spy.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/10 10:46:11 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/10 15:16:36 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

char	*ft_strlowcase(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (str[i] >= 'A' && str[i] <= 'Z')
			str[i] += ('a' - 'A');
		i++;
	}
	return (str);
}

int		ft_strcmp(char *s1, char *s2)
{
	while (*s1 == ' ')
		s1++;
	while (*s1 && (*s1 == *s2) && *s1 != ' ')
	{
		s1++;
		s2++;
	}
	if (*s1 < *s2)
		return (-1);
	else if (*s1 > *s2)
		return (1);
	else
		return (0);
}

int		ft_check(int argc, char **argv)
{
	int		i;
	char	*c;
	int		j;

	i = 0;
	while (i < argc)
	{
		c = ft_strlowcase(argv[i]);
		j = 0;
		if (!ft_strcmp(c, "president") || !ft_strcmp(c, "attack")
			|| !ft_strcmp(c, "bauer"))
			return (1);
		i++;
	}
	return (0);
}

void	ft_putstr(char *str)
{
	while (*str)
	{
		write(1, str, 1);
		str++;
	}
}

int		main(int argc, char *argv[])
{
	if (ft_check(argc, argv))
		ft_putstr("Alert!!!\n");
	return (0);
}
