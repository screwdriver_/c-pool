/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_collatz_conjecture.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/10 01:22:42 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/10 10:40:37 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

unsigned int	ft_collatz(unsigned int base)
{
	if (base % 2 == 0)
		return (base / 2);
	else
		return (base * 3 + 1);
}

unsigned int	ft_collatz_conjecture(unsigned int base)
{
	unsigned int n;
	unsigned int r;

	n = 1;
	if ((r = ft_collatz(base)) != 1)
		n += ft_collatz_conjecture(r);
	return (n);
}
