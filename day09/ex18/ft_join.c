/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_join.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/10 15:28:31 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/10 15:45:47 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

char	*ft_join(char **tab, char *sep)
{
	unsigned int	count;
	unsigned int	total_size;
	char			*s;
	unsigned int	i;
	unsigned int	j;

	count = 0;
	total_size = 0;
	while (tab[i])
	{
		total_size += ft_strlen(tab[i]);
		count++;
	}
	count--;
	s = (char*)malloc(total_size + 1);
	i = 0;
	j = 0;
	while (i < count)
	{
		while (tab[i])
		{
			s[j] = *(tab[i]);
			j++;
			tab[i]++;
		}
		// TODO
		i++;
	}
	return (s);
}
