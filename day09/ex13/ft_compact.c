/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_compact.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/10 15:10:08 by llenotre          #+#    #+#             */
/*   Updated: 2018/08/10 15:16:06 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_swap(char *a, char *b)
{
	char c;

	c = *a;
	*a = *b;
	*b = c;
}

int		ft_compact(char **tab, int length)
{
	int i;
	int j;

	i = 0;
	while (i < length)
	{
		if ((*tab)[i] != 0)
			continue ;
		j = 0;
		while ((*tab)[i + j] == 0)
			j++;
		ft_swap((*tab)[i], (*tab)[i + j]);
	}
}
